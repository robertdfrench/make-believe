include lib.mk

.PHONY: clean

tondawg: main.c ## build the program
	$(CC) -o $@ $<

test: tondawg ## run the tests
	$(MAKE) -C subfolder junk
	@./tondawg

clean: ## clean shit up
	rm -rf tondawg


lua := $(call search_path,lua)

print_lua_version: $(lua)  ## Depends on Lua
	$(lua) -v
