SHELL=bash

# This work of genius came from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


search_path=$(shell which $(1) || echo ".missing.$(1)")
.missing.%:
	$(error "You need to install $*")
